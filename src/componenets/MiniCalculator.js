import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sum: 0
    }
  }

  addOne = () => {
    this.setState({ sum: this.state.sum + 1 })
  }

  minusOne = () => {
    this.setState({ sum: this.state.sum - 1 })
  }

  multiplyTwo = () => {
    this.setState({ sum: this.state.sum * 2 })
  }

  divideTwo = () => {
    this.setState({ sum: this.state.sum / 2 })
  }

  render() {
    const { sum } = this.state
    const { addOne ,minusOne ,multiplyTwo ,divideTwo } = this

    return (
      <section>
        <div>计算结果为:
          <span className="result">{ sum }</span>
        </div>
        <div className="operations">
          <button onClick={addOne}>加1</button>
          <button onClick={minusOne}>减1</button>
          <button onClick={multiplyTwo}>乘以2</button>
          <button onClick={divideTwo}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;
